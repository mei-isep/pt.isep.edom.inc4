package pt.isep.edom.inc4;

public class Administrator {

	public static void execute() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		
		boolean exit=false;
		while (!exit) {
			System.out.println("Adminstrator");
		
			System.out.println("Select one option");
			System.out.println("0- Exit");
			System.out.println("1- Manage Contacts");
	
			// Read an integer from the input
			int num = in.nextInt();	
			switch (num) {
			case 1:
				{
					ManageContacts.execute();
				}
				break;
			case 0:
				exit=true;
			}
		}		
	}
}
