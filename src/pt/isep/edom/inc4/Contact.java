package pt.isep.edom.inc4;

import java.util.HashMap;

public class Contact {
	
	private static HashMap<Integer, String>map=new HashMap<Integer, String>();
	private static int lastId=0;
	
	public static int create(String name) {
		++lastId;
		map.put(lastId, name);
		return lastId;
	}

	public static String read(int id) {
		return map.get(id);
	}
	
	public static boolean update(int id, String name) {
		if (map.get(id)!=null) {
			map.put(id, name);
			return true;
		}
		else {
			return false;
		}
	}
	
	public static boolean delete(int id) {
		if (map.get(id)!=null) {
			map.remove(id);
			return true;
		}
		else {
			return false;
		}		
	}
}
