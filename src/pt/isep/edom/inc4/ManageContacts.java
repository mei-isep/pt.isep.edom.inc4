package pt.isep.edom.inc4;

public class ManageContacts {

	public static void create() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("Please enter the name for the new Contact:");
		String name=in.nextLine();	
		
		int id=Contact.create(name);
		System.out.println("Contact created with id:"+Integer.toString(id));
	}
	
	public static void read() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("Please enter the id for the Contact:");
		int id=in.nextInt();	
		
		String name=Contact.read(id);
		System.out.println("The name for the Contact:"+name);		
	}
	
	public static void update() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("Please enter the id for the Contact:");
		int id=in.nextInt();	
		System.out.println("Please enter the name for the Contact:");
		String name=in.nextLine();	
		
		boolean result=Contact.update(id, name);
		if (result=true)
			System.out.println("The contact was updated");	
		else
			System.out.println("The contact was NOT updated!");	
	}
	
	public static void delete() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("Please enter the id for the Contact:");
		int id=in.nextInt();	
		
		boolean result=Contact.delete(id);	
		if (result=true)
			System.out.println("The contact was deleted");	
		else
			System.out.println("The contact was NOT deleted!");		
	}

	public static void execute() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		
		boolean exit=false;
		while (!exit) {
			System.out.println("Manage Contacts");
		
			System.out.println("Select one option");
			System.out.println("0- Exit");
			System.out.println("1- Create");
			System.out.println("2- Read");
			System.out.println("3- Update");	
			System.out.println("4- Delete");			
			
			// Read an integer from the input
			int num = in.nextInt();	
			switch (num) {
			case 1:
				{
					create();
				}
				break;
			case 2:
				{
					read();
				}
				break;	
			case 3:
				{
					update();
				}
				break;	
			case 4:
				{
					delete();
				}
				break;			
			case 0:
				exit=true;
			}
		}		
	}
}
