package pt.isep.edom.inc4;

public class Main {

	public static void main(String[] args) {
		java.util.Scanner in = new java.util.Scanner(System.in);
		
		boolean exit=false;
		while (!exit) {
			System.out.println("AgendaFrontEndApp");
		
			System.out.println("Select one option");
			System.out.println("0- Exit");
			System.out.println("1- Administrator");
	
			// Read an integer from the input
			int num = in.nextInt();	
			switch (num) {
			case 1:
				{
					Administrator.execute();
				}
				break;
			case 0:
				exit=true;
			}
		}	
	}
}
